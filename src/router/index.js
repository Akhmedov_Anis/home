import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Enter from '../views/Enter.vue'
import Register from '../views/Register.vue'
import Trans from '../views/Trans.vue'
import Add from '../views/Add.vue'
import MoreCard from '../views/Morecard.vue'
import MyTrans from '../views/MyTrans.vue'
import AddWallet from '../views/AddWallet.vue'

Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/Register',
        name: 'Register',
        component: Register
    },
    {
        path: '/Enter',
        name: 'Enter',
        component: Enter
    },
    {
        path: '/Add',
        name: 'Add',
        component: Add
    },
    {
        path: '/Trans',
        name: 'Trans',
        component: Trans
    },
    {
        path: '/MoreCard',
        name: 'MoreCard',
        component: MoreCard
    },
    {
        path: '/MyTrans',
        name: 'MyTrans',
        component: MyTrans
    },
    {
        path: '/AddWallet',
        name: 'AddWallet',
        component: AddWallet
    },
]

const router = new VueRouter({
    routes
})

export default router
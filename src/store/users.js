import Vue from "vue";
import Vuex from "vuex";
import api from "../api/routers.js";
import router from "../router/index.js";

Vue.use(Vuex);

const state = () => ({
    data: [],
    bool: false,
    user: [],
    routes: {
        register: {
            api: "auth/signup"
        },
        login: {
            api: "auth/login"
        },
    },
});

const getters = {
    user: state => JSON.parse(localStorage.user),
    isBool: (state) => state.bool
};

const mutations = {
    CHANGEBOOL(state){
        state.bool = true
        console.log(state.bool);
        setTimeout(() => {
            state.bool = false
        }, 6000);
        console.log(state.bool);
    }
}

const actions = {
    FETCH_USERS({ commit }) {
        api.get({ api: state().routes.getALL.api })
            .then(res => {
                if (res.status == 200 || res.status == 201 || res.status == 202) {
                    // commit("POPULATE_USERS", res.data)
                }
            })
            .catch(err => {
                console.log(err);
            })
    },
    CHECK_ME({ commit }) {
        if (!localStorage.token) {
            router.push({ name: "Register" })
        }
    },
    REGISTER({ commit }) {
        event.preventDefault()

        let obj = {}
        let fm = new FormData(event.target)

        fm.forEach((value, key) => {
            obj[key] = value
        })

        api.post({ api: state().routes.register.api, obj })
            .then(res => {
                if (res.status == 200 || res.status == 201 || res.status == 202) {
                    localStorage.user = JSON.stringify(res.data)
                    localStorage.token = res.data.token
                    
                    commit("CHANGEBOOL")
                    
                    setTimeout(() => {
                        router.push({ name: "Home" })
                    }, 5000);
                }
            })
            .catch(err => {
                console.log(err);
            })
    },


    GOON({ commit }) {
        event.preventDefault()

        let obj = {}
        let fm = new FormData(event.target)

        fm.forEach((value, key) => {
            obj[key] = value
        })

        api.post({ api: state().routes.login.api, obj })
            .then(res => {
                if (res.status == 200 || res.status == 201 || res.status == 202) {
                    localStorage.user = JSON.stringify(res.data)
                    localStorage.token = res.data.token
                    
                    commit("CHANGEBOOL")
                    
                    setTimeout(() => {
                        router.push({ name: "Home" })
                    }, 5000);
                }
            })
            .catch(err => {
                console.log(err);
            })
    },
    GOOUT({ commit }) {
        localStorage.clear()
        router.push({ name: "Register" })
    }

};

export default {
    state,
    getters,
    mutations,
    actions,
};